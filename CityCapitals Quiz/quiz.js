
var correct = 0;
var incorrect = 0;
var responses = [];
var correctAnswers = [];
var incorrectAnswers = [];
var quizQandA = [
  ['What is the capital of Texas?', 'Austin'],
  ['What is the capital of California?', 'Sacramento'],
  ['What is the capital of New York?', 'Albany'],
  ['What is the capital of Colorado?', 'Denver'],
  ['What is the capital of Minnesota?', 'Minneapolis/St. Paul']
];

// Displays items within array. Creates a list item that stores
function viewArray( array ) {
  var HTML = '<ol>';
  for ( i = 0; i < array.length; i += 1 ) {
    HTML += '<li>' + array[i] + '</li> <br />';
  }
  HTML += '</ol>';
  return HTML;
}

// Prompts user to submit response
function askQuestion() {
  for ( i = 0; i < quizQandA.length; i += 1 ) {
    var response = prompt( quizQandA[i][0]);
    responses.push(response);
  }
}

// Receives each user response and compares it to the correct answer. Returns + or - point based on answer.
function grade() {
  for ( i = 0; i < quizQandA.length; i += 1 ) {
    if ( responses[i] === quizQandA[i][1] ) {
      correctAnswers.push(responses[i]);
      correct += 1;
    }
    else {
      incorrectAnswers.push(responses[i]);
      incorrect += 1;
    }
  }
}

function displayResults() {
  var outputDiv = document.getElementById('output');
  outputDiv.innerHTML = ('<h2>You have answered ' + correct + ' questions correctly and ' + incorrect + ' incorrectly. <br /></h2>');
  outputDiv.innerHTML = ('<h2>Questions answered correctly: ' + '<br />' + viewArray(correctAnswers) + '<br />' + ' and incorrectly: ' + '<br />' + viewArray(incorrectAnswers) + '</h2>');
}

// Cleaner way to display the above:
/*
html = "You got " + correctAnswers + " question(s) right."
html += '<h2>You got these questions correct:</h2>';
html += buildList(correct);
html += '<h2>You go these questions wrong:</h2>';
html += buildList(wrong);
print(html);
*/

askQuestion();
grade();
displayResults();